Для того, чтобы заработали утилиты cp и ls в директории, которую мы сделали корневой с помощью chroot, нужно (предварительно была создана в хомяке дира less1.3 и туда скопирован бинарь bash и необходимые зависимости), нужно:
1. скопировать бинарники cp и ls в нашу новую корневую директорию
```
cp /bin/cp less1.3/bin
cp /bin/cp less1.3/bin
```
2. если мы сейчас выполним chroot less1.3 и затем попытаемся выполнить cp и ls, то получим ошибки, потому что необходимо также скопировать все необходимые зависимостия для данных утилит, находим зависимости:
```
root@novoleg-vm-00:~# ldd /bin/cp
	linux-vdso.so.1 (0x00007ffe775c0000)
	libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f57db568000)
	libacl.so.1 => /lib/x86_64-linux-gnu/libacl.so.1 (0x00007f57db55d000)
	libattr.so.1 => /lib/x86_64-linux-gnu/libattr.so.1 (0x00007f57db555000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f57db394000)
	libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f57db320000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f57db31b000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f57db7bb000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f57db2f8000)
root@novoleg-vm-00:~# ldd /bin/ls
	linux-vdso.so.1 (0x00007ffe05b7a000)
	libselinux.so.1 => /lib/x86_64-linux-gnu/libselinux.so.1 (0x00007f3154b4b000)
	libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f315498a000)
	libpcre.so.3 => /lib/x86_64-linux-gnu/libpcre.so.3 (0x00007f3154916000)
	libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007f3154911000)
	/lib64/ld-linux-x86-64.so.2 (0x00007f3154d9d000)
	libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007f31548f0000)
```

выполняем копированивае недостающих библиотек, снова выполняем chroot less1.3 и проверяем, что cp и ls выполняются:
```root@novoleg-vm-00:~# chroot less1.3/
bash-5.0# cp --help
Usage: cp [OPTION]... [-T] SOURCE DEST
  or:  cp [OPTION]... SOURCE... DIRECTORY
  or:  cp [OPTION]... -t DIRECTORY SOURCE...
Copy SOURCE to DEST, or multiple SOURCE(s) to DIRECTORY.

Mandatory arguments to long options are mandatory for short options too.

bash-5.0# ls -l
total 12
drwxr-xr-x 2 0 0 4096 Nov  1 17:10 bin
drwxr-xr-x 2 0 0 4096 Nov  1 17:18 lib
drwxr-xr-x 2 0 0 4096 Nov  1 17:09 lib64
```

как видно команды выполняются из изменненой корневой директории