# Ответы на вопросы

1. от процесса bash создается дочерний процесс `ls -l`. Увидеть, что процесс `ls -l` я смог с помощью команды `watch ls -l` и получил следующий вывод:
```bash
novoleg+  6589  6583  0 07:40 ?        S      0:00      \_ sshd: novoleg2697@pts/1
novoleg+  6590  6589  0 07:40 pts/1    Ss     0:00          \_ -bash
root      6593  6590  0 07:40 pts/1    S      0:00              \_ sudo -i
root      6594  6593  0 07:40 pts/1    S      0:00                  \_ -bash
root      7072  6594  0 07:51 pts/1    S+     0:00                      \_ watch ls -l
```
P.S. правильный ответ на данный вопрос:

```
При выполнении команды в консоли происходит системный вызов fork(), в результате которого создаётся копия процесса консоли, затем копия процесса выполняет команду с помощью системного вызова exec(). После выполнения команды, копия процесса выполняет системный вызов exit(), в результате которого оригинальному процессу консоли отправляется сигнал SIGCHLD (сообщающий о том, что дочерний процесс завершён). Во время работы копии процесса, оригинальный процесс находится в ожидании из-за системного вызова wait().
```

2. выполнил с помощью команды `ps -eo pid,user,%cpu,vsz,tt,cmd`, результат:
```bash
ps -eo pid,user,%cpu,vsz,tt,cmd
  PID USER     %CPU    VSZ TT       CMD
    1 root      0.0 103964 ?        /sbin/init
    2 root      0.0      0 ?        [kthreadd]
    3 root      0.0      0 ?        [rcu_gp]
    4 root      0.0      0 ?        [rcu_par_gp]
    6 root      0.0      0 ?        [kworker/0:0H-kblockd]
    7 root      0.0      0 ?        [kworker/u4:0-events_unbound]
    8 root      0.0      0 ?        [mm_percpu_wq]
    9 root      0.0      0 ?        [ksoftirqd/0]
   10 root      0.0      0 ?        [rcu_sched]
   11 root      0.0      0 ?        [rcu_bh]
   12 root      0.0      0 ?        [migration/0]
   14 root      0.0      0 ?        [cpuhp/0]
   15 root      0.0      0 ?        [cpuhp/1]
```

3. создал процесс `$ sleep infinity &`
- убедимся, что процесс существует 
```
ps aux | grep sleep
root      7317  0.0  0.0   4048   744 pts/1    S    07:58   0:00 sleep infinity
root      7319  0.0  0.0   4836   888 pts/1    S+   07:58   0:00 grep sleep
root@novoleg-vm-00:~#
```
- выводим только процесс sleep
```
ps aux | grep sleep | grep -v 'grep'
root      7317  0.0  0.0   4048   744 pts/1    S    07:58   0:00 sleep infinity
```
- убиваем его и убеждаемся, что его нет. В выводе есть сообщение `Terminated` - это означает, что процессу для его завершения был послан сигнал SIGTERM, т.е. мягкое завершение процесса. Чтобы послать процессу сиганал SIGKILL нужно добавить ключик `kill -9`
```
 kill 7317
root@novoleg-vm-00:~# ps aux | grep sleep | grep -v 'grep'
[1]+  Terminated              sleep infinity
root@novoleg-vm-00:~# ps aux | grep sleep | grep -v 'grep'
root@novoleg-vm-00:~#
```